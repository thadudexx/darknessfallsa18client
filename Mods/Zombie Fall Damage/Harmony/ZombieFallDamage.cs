using DMT;
using Harmony;
using InControl;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using UnityEngine;

class Khaine_EntityZombie_Patch
{
    public class Khaine_EntityZombie_Logger
    {
        public static bool blDisplayLog = true;

        public static void Log(String strMessage)
        {
            if (blDisplayLog)
                UnityEngine.Debug.Log(strMessage);
        }
    }

    public class Khaine_EntityZombie_Init : IHarmony
    {
        public void Start()
        {
            Khaine_EntityZombie_Logger.Log(" Loading Patch: " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(EntityZombie))]
    [HarmonyPatch("DamageEntity")]
    public class Khaine_EntityZombieDamageEntity
    {
        // Loops around the instructions and removes the return condition.
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        {
            int counter = 0;

            // Grab all the instructions
            var codes = new List<CodeInstruction>(instructions);
            for (int i = 0; i < codes.Count; i++)
            {
                if (codes[i].opcode == OpCodes.Ldc_I4_2) // This removes the divider on the fall damage calc
                {
                    counter++;
                    Khaine_EntityZombie_Logger.Log("Adjusting 2 to 1"); 

                    codes[i].opcode = OpCodes.Ldc_I4; // convert to the right thing
                    codes[i].operand = 1;
                    Khaine_EntityZombie_Logger.Log("Done with 2 to 1");
                }
                if (codes[i].opcode == OpCodes.Ldc_I4_3) // This removes the cap on zombie fall damage
                {
                    counter++;
                    Khaine_EntityZombie_Logger.Log("Adjusting 3 to 1"); 

                    codes[i].opcode = OpCodes.Ldc_I4; // convert to the right thing
                    codes[i].operand = 1;
                    Khaine_EntityZombie_Logger.Log("Done with 3 to 1");
                }
            }
            return codes.AsEnumerable();
        }
    }
}